# Import PCL module
import pcl


#################### Voxel Filter ##########################
def voxel_filter(cloud, save_file=True, leaf_size=0.01):
    # Create a VoxelGrid filter object for our input point cloud
    vox = cloud.make_voxel_grid_filter()

    # Set the voxel (or leaf) size
    vox.set_leaf_size(leaf_size, leaf_size, leaf_size)

    # Call the filter function to obtain the resultant downsampled point cloud
    cloud_filtered = vox.filter()
    if save_file:
        filename = 'voxel_downsampled.pcd'
        pcl.save(cloud_filtered, filename)

    return cloud_filtered


#################### PassThrough Filter ##########################
def passthrough_filter(cloud,
                       save_file=True,
                       filter_axis='z',
                       axis_min=0.77,
                       axis_max=1.2):
    # Create a PassThrough filter object.
    passthrough = cloud.make_passthrough_filter()

    # Assign axis and range to the passthrough filter object.
    passthrough.set_filter_field_name(filter_axis)
    passthrough.set_filter_limits(axis_min, axis_max)

    # Finally use the filter function to obtain the resultant point cloud.
    cloud_filtered = passthrough.filter()
    if save_file:
        filename = 'passthrough_filtered.pcd'
        pcl.save(cloud_filtered, filename)

    return cloud_filtered


#################### RANSAC plane segmentation ####################
def RANSAC_plane_segmentation(cloud, max_distance=0.01):
    seg = cloud.make_segmenter()
    seg.set_model_type(pcl.SACMODEL_PLANE)
    seg.set_method_type(pcl.SAC_RANSAC)
    seg.set_distance_threshold(max_distance)
    inliers, coefficients = seg.segment()
    return inliers, coefficients


def extract_indices(cloud, inliers, negative=False, save_file=True):
    extracted_indices = cloud.extract(inliers, negative=negative)
    if save_file:
        filename = 'extracted_{}.pcd'.format(
            'inliers' if not negative else 'outliers')
        pcl.save(extracted_indices, filename)

    return extracted_indices


def filter_noise(cloud, mean_k=50, x=1, save_file=True):
    # Save pcd for tabletop objects
    outlier_filter = cloud.make_statistical_outlier_filter()
    outlier_filter.set_mean_k(mean_k)
    outlier_filter.set_std_dev_mul_thresh(x)
    noise_filtered = outlier_filter.filter()
    if save_file:
        filename = 'noise_filtered.pcd'
        pcl.save(noise_filtered, filename)
    return noise_filtered


if __name__ == '__main__':
    # Load Point Cloud file
    cloud = pcl.load_XYZRGB('tabletop.pcd')

    voxel_filtered_cloud = voxel_filter(cloud)

    passthrough_filtered_cloud = passthrough_filter(voxel_filtered_cloud)

    inliers, coefficients = RANSAC_plane_segmentation(passthrough_filtered_cloud)

    extracted_inliers = extract_indices(
        passthrough_filtered_cloud, inliers, negative=False)

    extracted_outliers = extract_indices(
        passthrough_filtered_cloud, inliers, negative=True)
