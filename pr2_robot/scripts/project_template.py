#!/usr/bin/env python

# Import modules
import numpy as np
import sklearn
from sklearn.preprocessing import LabelEncoder
import pickle
import collections
from sensor_stick.srv import GetNormals
from sensor_stick.features import compute_color_histograms
from sensor_stick.features import compute_normal_histograms
from visualization_msgs.msg import Marker
from sensor_stick.marker_tools import *
from sensor_stick.msg import DetectedObjectsArray
from sensor_stick.msg import DetectedObject
from sensor_stick.pcl_helper import *

import rospy
import tf
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64
from std_msgs.msg import Int32
from std_msgs.msg import String
from pr2_robot.srv import *
from rospy_message_converter import message_converter
import yaml

from RANSAC import voxel_filter
from RANSAC import passthrough_filter
from RANSAC import RANSAC_plane_segmentation
from RANSAC import extract_indices
from RANSAC import filter_noise


ArmDropbox = collections.namedtuple('ArmDropbox', ['name', 'group', 'pos'])


TEST_WORLD = 3


# Helper function to get surface normals
def get_normals(cloud):
    get_normals_prox = rospy.ServiceProxy('/feature_extractor/get_normals', GetNormals)
    return get_normals_prox(cloud).cluster


# Helper function to create a yaml friendly dictionary from ROS messages
def make_yaml_dict(test_scene_num, arm_name, object_name, pick_pose, place_pose):
    yaml_dict = {}
    yaml_dict["test_scene_num"] = test_scene_num.data
    yaml_dict["arm_name"]  = arm_name.data
    yaml_dict["object_name"] = object_name.data
    yaml_dict["pick_pose"] = message_converter.convert_ros_message_to_dictionary(pick_pose)
    yaml_dict["place_pose"] = message_converter.convert_ros_message_to_dictionary(place_pose)
    return yaml_dict


# Helper function to output to yaml file
def send_to_yaml(yaml_filename, dict_list):
    data_dict = {"object_list": dict_list}
    import json
    data_dict = json.loads(json.dumps(data_dict, indent=2))
    with open(yaml_filename, 'w') as outfile:
        yaml.dump(data_dict, outfile, default_flow_style=False)


# Callback function for your Point Cloud Subscriber
def pcl_callback(pcl_msg):
    debug = False

    # Convert ROS msg to PCL data
    cloud = ros_to_pcl(pcl_msg)

    # Voxel Grid Downsampling
    voxel_filtered_cloud = voxel_filter(
        cloud, save_file=debug, leaf_size=0.003)

    # PassThrough Filter
    passthrough_filtered_cloud = passthrough_filter(
        voxel_filtered_cloud, save_file=debug, axis_min=0.63, axis_max=1.5)
    passthrough_filtered_cloud = passthrough_filter(
        passthrough_filtered_cloud, save_file=debug,
        filter_axis='y', axis_min=-0.5, axis_max=0.5)

    # RANSAC Plane Segmentation
    inliers, coefficients = RANSAC_plane_segmentation(passthrough_filtered_cloud)

    # Extract inliers and outliers
    noise_filtered_cloud = filter_noise(
        passthrough_filtered_cloud, save_file=debug, mean_k=50, x=1)
    pcl_table = extract_indices(
        noise_filtered_cloud, inliers, negative=False, save_file=debug)
    pcl_objects = extract_indices(
        noise_filtered_cloud, inliers, negative=True, save_file=debug)

    # Euclidean Clustering
    white_cloud = XYZRGB_to_XYZ(pcl_objects)
    tree = white_cloud.make_kdtree()
    # Create a cluster extraction object
    ec = white_cloud.make_EuclideanClusterExtraction()
    # Set tolerances for distance threshold 
    # as well as minimum and maximum cluster size (in points)
    ec.set_ClusterTolerance(0.01)
    # MinClusterSize = 450 for test_world_1
    # MinClusterSize = 550 for test_world_2
    ec.set_MinClusterSize(550)
    ec.set_MaxClusterSize(50000)
    # Search the k-d tree for clusters
    ec.set_SearchMethod(tree)
    # Extract indices for each of the discovered clusters
    cluster_indices = ec.Extract()

    # Create Cluster-Mask Point Cloud to visualize each cluster separately
    # Assign a color corresponding to each segmented object in scene
    # print('len(cluster_indices):', len(cluster_indices))
    cluster_color = get_color_list(len(cluster_indices))
    color_cluster_point_list = []
    for j, indices in enumerate(cluster_indices):
        for i, indice in enumerate(indices):
            color_cluster_point_list.append([white_cloud[indice][0],
                                             white_cloud[indice][1],
                                             white_cloud[indice][2],
                                             rgb_to_float(cluster_color[j])])
    #Create new cloud containing all clusters, each with unique color
    cluster_cloud = pcl.PointCloud_PointXYZRGB()
    cluster_cloud.from_list(color_cluster_point_list)

    # Convert PCL data to ROS messages
    ros_cloud_table = pcl_to_ros(pcl_table)
    ros_cloud_objects = pcl_to_ros(pcl_objects)
    ros_cluster_cloud = pcl_to_ros(cluster_cloud)

    # Publish ROS messages
    pcl_objects_pub.publish(ros_cloud_objects)
    pcl_table_pub.publish(ros_cloud_table)
    pcl_cluster_pub.publish(ros_cluster_cloud)
    print('published objects, table, and cluster')

    # Classify the clusters! (loop through each detected cluster one at a time)
    detected_objects_labels = []
    detected_objects = []
    detected_objects_pos = []

    for index, pts_list in enumerate(cluster_indices):
        # Grab the points for the cluster from the extracted outliers (cloud_objects)
        pcl_cluster = pcl_objects.extract(pts_list)

        # Convert the cluster from pcl to ROS using helper function
        ros_cluster = pcl_to_ros(pcl_cluster)

        # Compute the associated feature vector
        chists = compute_color_histograms(ros_cluster, using_hsv=True)
        normals = get_normals(ros_cluster)
        nhists = compute_normal_histograms(normals)
        feature = np.concatenate((chists, nhists))

        # Make the prediction
        feature = feature.reshape(1, -1)
        prediction = clf.predict(scaler.transform(feature))
        label = encoder.inverse_transform(prediction)[0]
        detected_objects_labels.append(label)

        # Publish a label into RViz
        label_pos = list(white_cloud[pts_list[0]])
        detected_objects_pos.append(label_pos)
        pcl_object_markers_pub.publish(make_label(label, label_pos, index))

        # Add the detected object to the list of detected objects.
        do = DetectedObject()
        do.label = label
        do.cloud = ros_cluster
        detected_objects.append(do)
    # End for index, pts_list in enumerate(cluster_indices)

    rospy.loginfo('Detected {} objects: {}'.format(
        len(detected_objects_labels), zip(detected_objects_labels,
                                          detected_objects_pos)))

    # Publish the list of detected objects
    pcl_detected_objects_pub.publish(detected_objects)

    # get parameters
    object_list_param = rospy.get_param('/object_list')
    pick_list_objects = [obj['name'] for obj in object_list_param]

    # Suggested location for where to invoke your pr2_mover() function within pcl_callback()
    # Could add some logic to determine whether or not your object detections are robust
    # before calling pr2_mover()
    pick_objects_set = set(pick_list_objects)
    detected_objects_set = set(detected_objects_labels)
    print 'Expecting:', pick_objects_set, 'Detected:', detected_objects_set
    if detected_objects_set <= pick_objects_set:
        try:
            pr2_mover(detected_objects)
        except rospy.ROSInterruptException:
            pass


# function to load parameters and request PickPlace service
def pr2_mover(object_list):

    # Initialize variables
    test_scene_num = Int32()
    test_scene_num.data = TEST_WORLD  # test_world id
    out_yaml = 'output_{}.yaml'.format(TEST_WORLD)

    # Get/Read parameters
    object_list_param = rospy.get_param('/object_list')

    box_group_position_map = {}
    box_param = rospy.get_param('/dropbox')
    for box in box_param:
        group = box['group']
        position = box['position']
        name = box['name']
        arm_box = ArmDropbox(name=name,
                             group=group,
                             pos=position)
        box_group_position_map[group] = arm_box

    # Parse parameters into individual variables
    object_name_group_map = {}
    for obj in object_list_param:
        name = obj['name']
        group = obj['group']
        object_name_group_map[name] = group

    # Loop through the pick list
    dict_list = []
    for obj in object_list:
        # Get the PointCloud for a given object and obtain it's centroid
        print 'processing: {}'.format(obj.label)
        points_arr = ros_to_pcl(obj.cloud).to_array()
        centroid = np.mean(points_arr, axis=0)[:3]

        group = object_name_group_map[obj.label]
        # Create 'place_pose' for the object
        place_pose = Pose()
        dst_pos = box_group_position_map[group].pos
        place_pose.position.x = dst_pos[0]
        place_pose.position.y = dst_pos[1]
        place_pose.position.z = dst_pos[2]

        # Assign the arm to be used for pick_place
        pick_pose = Pose()
        pick_pose.position.x = np.asscalar(centroid[0])
        pick_pose.position.y = np.asscalar(centroid[1])
        pick_pose.position.z = np.asscalar(centroid[2])
        arm_name = String()
        arm_name.data = box_group_position_map[group].name

        object_name = String()
        object_name.data = obj.label
        # Create a list of dictionaries (made with make_yaml_dict()) for later output to yaml format
        dict_list.append(make_yaml_dict(test_scene_num,
                                        arm_name,
                                        object_name,
                                        pick_pose,
                                        place_pose))
        # Wait for 'pick_place_routine' service to come up
        rospy.wait_for_service('pick_place_routine')

        try:
            pick_place_routine = rospy.ServiceProxy('pick_place_routine', PickPlace)

            # Insert your message variables to be sent as a service request
            resp = pick_place_routine(test_scene_num,
                                      object_name,
                                      arm_name,
                                      pick_pose,
                                      place_pose)

            print "Response:", resp.success

        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    # Output your request parameters into output yaml file
    send_to_yaml(out_yaml, dict_list)


if __name__ == '__main__':
    # ROS node initialization
    rospy.init_node('clustering', anonymous=True)

    # Create Subscribers
    pcl_sub = rospy.Subscriber(
        '/pr2/world/points', pc2.PointCloud2, pcl_callback,
        queue_size=1)

    # Create Publishers
    pcl_objects_pub = rospy.Publisher(
        '/pcl_objects', PointCloud2, queue_size=1)
    pcl_table_pub = rospy.Publisher(
        '/pcl_table', PointCloud2, queue_size=1)
    pcl_cluster_pub = rospy.Publisher(
        '/pcl_cluster', PointCloud2, queue_size=1)
    pcl_object_markers_pub = rospy.Publisher(
        '/object_markers', Marker, queue_size=1)
    pcl_detected_objects_pub = rospy.Publisher(
        '/detected_objects', DetectedObjectsArray, queue_size=1)

    # Load Model From disk
    model = pickle.load(open('model-{}.sav'.format(TEST_WORLD), 'rb'))
    clf = model['classifier']
    encoder = LabelEncoder()
    encoder.classes_ = model['classes']
    scaler = model['scaler']

    # Initialize color_list
    get_color_list.color_list = []

    # Spin while node is not shutdown
    while not rospy.is_shutdown():
        rospy.spin()

