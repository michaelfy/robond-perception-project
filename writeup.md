## Project: Perception Pick & Place
### Writeup Template: You can use this file as a template for your writeup if you want to submit it as a markdown file, but feel free to use some other method and submit a pdf if you prefer.

---


# Required Steps for a Passing Submission:
1. Extract features and train an SVM model on new objects (see `pick_list_*.yaml` in `/pr2_robot/config/` for the list of models you'll be trying to identify). 
2. Write a ROS node and subscribe to `/pr2/world/points` topic. This topic contains noisy point cloud data that you must work with.
3. Use filtering and RANSAC plane fitting to isolate the objects of interest from the rest of the scene.
4. Apply Euclidean clustering to create separate clusters for individual items.
5. Perform object recognition on these objects and assign them labels (markers in RViz).
6. Calculate the centroid (average in x, y and z) of the set of points belonging to that each object.
7. Create ROS messages containing the details of each object (name, pick_pose, etc.) and write these messages out to `.yaml` files, one for each of the 3 scenarios (`test1-3.world` in `/pr2_robot/worlds/`).  [See the example `output.yaml` for details on what the output should look like.](https://github.com/udacity/RoboND-Perception-Project/blob/master/pr2_robot/config/output.yaml)  
8. Submit a link to your GitHub repo for the project or the Python code for your perception pipeline and your output `.yaml` files (3 `.yaml` files, one for each test world).  You must have correctly identified 100% of objects from `pick_list_1.yaml` for `test1.world`, 80% of items from `pick_list_2.yaml` for `test2.world` and 75% of items from `pick_list_3.yaml` in `test3.world`.
9. Congratulations!  Your Done!

# Extra Challenges: Complete the Pick & Place
7. To create a collision map, publish a point cloud to the `/pr2/3d_map/points` topic and make sure you change the `point_cloud_topic` to `/pr2/3d_map/points` in `sensors.yaml` in the `/pr2_robot/config/` directory. This topic is read by Moveit!, which uses this point cloud input to generate a collision map, allowing the robot to plan its trajectory.  Keep in mind that later when you go to pick up an object, you must first remove it from this point cloud so it is removed from the collision map!
8. Rotate the robot to generate collision map of table sides. This can be accomplished by publishing joint angle value(in radians) to `/pr2/world_joint_controller/command`
9. Rotate the robot back to its original state.
10. Create a ROS Client for the “pick_place_routine” rosservice.  In the required steps above, you already created the messages you need to use this service. Checkout the [PickPlace.srv](https://github.com/udacity/RoboND-Perception-Project/tree/master/pr2_robot/srv) file to find out what arguments you must pass to this service.
11. If everything was done correctly, when you pass the appropriate messages to the `pick_place_routine` service, the selected arm will perform pick and place operation and display trajectory in the RViz window
12. Place all the objects from your pick list in their respective dropoff box and you have completed the challenge!
13. Looking for a bigger challenge?  Load up the `challenge.world` scenario and see if you can get your perception pipeline working there!

## [Rubric](https://review.udacity.com/#!/rubrics/1067/view) Points
### Here I will consider the rubric points individually and describe how I addressed each point in my implementation.  

[//]: # (Image References)
[world_1_voxel]: ./misc_images/world-1-voxel-downsampled.png
[world_1_pass]: ./misc_images/world-1-passthrough-filtered.png
[world_1_noise]: ./misc_images/world-1-noise-filtered.png
[world_1_final]: ./misc_images/world-1-detection.png
[world_1_matrix_raw]: ./misc_images/world-1-confusion-matrix-raw.png
[world_1_matrix_norm]: ./misc_images/world-1-confusion-matrix-normalized.png
[world_2_voxel]: ./misc_images/world-2-voxel-downsampled.png
[world_2_pass]: ./misc_images/world-2-passthrough-filtered.png
[world_2_noise]: ./misc_images/world-2-noise-fitered.png
[world_2_final]: ./misc_images/world-2-detection.png
[world_2_matrix_raw]: ./misc_images/world-2-confusion-matrix-raw.png
[world_2_matrix_norm]: ./misc_images/world-2-confusion-matrix-normalized.png
[world_3_voxel]: ./misc_images/world-3-voxel-downsampled.png
[world_3_pass]: ./misc_images/world-3-passthrough-filtered.png
[world_3_noise]: ./misc_images/world-3-noise-filtered.png
[world_3_final]: ./misc_images/world-3-detection.png
[world_3_matrix_raw]: ./misc_images/world-3-confusion-matrix-raw.png
[world_3_matrix_norm]: ./misc_images/world-3-confusion-matrix-normalized.png

---
### Writeup / README

#### 1. Provide a Writeup / README that includes all the rubric points and how you addressed each one.  You can submit your writeup as markdown or pdf.  

You're reading it!

### Exercise 1, 2 and 3 pipeline implemented
#### 1. Complete Exercise 1 steps. Pipeline for filtering and RANSAC plane fitting implemented.
I have refactored the implementations of different filters into functions. See `pr2_robot/scripts/RANSAC.py` or the following code snippet
```python
#################### Voxel Filter ##########################
def voxel_filter(cloud, save_file=True, leaf_size=0.01):
    # Create a VoxelGrid filter object for our input point cloud
    vox = cloud.make_voxel_grid_filter()

    # Set the voxel (or leaf) size
    vox.set_leaf_size(leaf_size, leaf_size, leaf_size)

    # Call the filter function to obtain the resultant downsampled point cloud
    cloud_filtered = vox.filter()
    if save_file:
        filename = 'voxel_downsampled.pcd'
        pcl.save(cloud_filtered, filename)

    return cloud_filtered


#################### PassThrough Filter ##########################
def passthrough_filter(cloud,
                       save_file=True,
                       filter_axis='z',
                       axis_min=0.77,
                       axis_max=1.2):
    # Create a PassThrough filter object.
    passthrough = cloud.make_passthrough_filter()

    # Assign axis and range to the passthrough filter object.
    passthrough.set_filter_field_name(filter_axis)
    passthrough.set_filter_limits(axis_min, axis_max)

    # Finally use the filter function to obtain the resultant point cloud.
    cloud_filtered = passthrough.filter()
    if save_file:
        filename = 'passthrough_filtered.pcd'
        pcl.save(cloud_filtered, filename)

    return cloud_filtered


#################### RANSAC plane segmentation ####################
def RANSAC_plane_segmentation(cloud, max_distance=0.01):
    seg = cloud.make_segmenter()
    seg.set_model_type(pcl.SACMODEL_PLANE)
    seg.set_method_type(pcl.SAC_RANSAC)
    seg.set_distance_threshold(max_distance)
    inliers, coefficients = seg.segment()
    return inliers, coefficients


def extract_indices(cloud, inliers, negative=False, save_file=True):
    extracted_indices = cloud.extract(inliers, negative=negative)
    if save_file:
        filename = 'extracted_{}.pcd'.format(
            'inliers' if not negative else 'outliers')
        pcl.save(extracted_indices, filename)

    return extracted_indices


def filter_noise(cloud, mean_k=50, x=1, save_file=True):
    # Save pcd for tabletop objects
    outlier_filter = cloud.make_statistical_outlier_filter()
    outlier_filter.set_mean_k(mean_k)
    outlier_filter.set_std_dev_mul_thresh(x)
    noise_filtered = outlier_filter.filter()
    if save_file:
        filename = 'noise_filtered.pcd'
        pcl.save(noise_filtered, filename)
    return noise_filtered
```
The RANSAC implemntation in `def pcl_callback` is as follows:
```python
    # Convert ROS msg to PCL data
    cloud = ros_to_pcl(pcl_msg)

    # Voxel Grid Downsampling
    voxel_filtered_cloud = voxel_filter(
        cloud, save_file=debug, leaf_size=0.003)

    # PassThrough Filter
    passthrough_filtered_cloud = passthrough_filter(
        voxel_filtered_cloud, save_file=debug, axis_min=0.63, axis_max=1.5)
    passthrough_filtered_cloud = passthrough_filter(
        passthrough_filtered_cloud, save_file=debug,
        filter_axis='y', axis_min=-0.5, axis_max=0.5)

    # RANSAC Plane Segmentation
    inliers, coefficients = RANSAC_plane_segmentation(passthrough_filtered_cloud)

    # Extract inliers and outliers
    noise_filtered_cloud = filter_noise(
        passthrough_filtered_cloud, save_file=debug, mean_k=50, x=1)
    pcl_table = extract_indices(
        noise_filtered_cloud, inliers, negative=False, save_file=debug)
    pcl_objects = extract_indices(
        noise_filtered_cloud, inliers, negative=True, save_file=debug)

```

#### 2. Complete Exercise 2 steps: Pipeline including clustering for segmentation implemented.  
The implementation of clustering is as follows:
```python
    # Euclidean Clustering
    white_cloud = XYZRGB_to_XYZ(pcl_objects)
    tree = white_cloud.make_kdtree()
    # Create a cluster extraction object
    ec = white_cloud.make_EuclideanClusterExtraction()
    # Set tolerances for distance threshold 
    # as well as minimum and maximum cluster size (in points)
    ec.set_ClusterTolerance(0.01)
    # MinClusterSize = 450 for test_world_1
    # MinClusterSize = 550 for test_world_2
    ec.set_MinClusterSize(550)
    ec.set_MaxClusterSize(50000)
    # Search the k-d tree for clusters
    ec.set_SearchMethod(tree)
    # Extract indices for each of the discovered clusters
    cluster_indices = ec.Extract()

    # Create Cluster-Mask Point Cloud to visualize each cluster separately
    # Assign a color corresponding to each segmented object in scene
    # print('len(cluster_indices):', len(cluster_indices))
    cluster_color = get_color_list(len(cluster_indices))
    color_cluster_point_list = []
    for j, indices in enumerate(cluster_indices):
        for i, indice in enumerate(indices):
            color_cluster_point_list.append([white_cloud[indice][0],
                                             white_cloud[indice][1],
                                             white_cloud[indice][2],
                                             rgb_to_float(cluster_color[j])])
    #Create new cloud containing all clusters, each with unique color
    cluster_cloud = pcl.PointCloud_PointXYZRGB()
    cluster_cloud.from_list(color_cluster_point_list)
```

#### 3. Complete Exercise 3 Steps.  Features extracted and SVM trained.  Object recognition implemented.

Confusion Matrices:

 1. World-1: Without normalization ![world_1_matrix_raw][world_1_matrix_raw]
 1. World-1: With normalization ![world_1_matrix_norm][world_1_matrix_norm]
 1. World-2: Without normalization ![world_2_matrix_raw][world_2_matrix_raw]
 1. World-2: With normalization ![world_2_matrix_norm][world_2_matrix_norm]
 1. World-3: Without normalization ![world_3_matrix_raw][world_3_matrix_raw]
 1. World-3: With normalization ![world_3_matrix_norm][world_3_matrix_norm]


The implementation of feature extraction is as follows (or see `robond-perception-exercises/src/master/Exercise-3/sensor_stick/src/sensor_stick/features.py`):
```python
def compute_color_histograms(cloud, using_hsv=False):

    # Compute histograms for the clusters
    point_colors_list = []

    # Step through each point in the point cloud
    for point in pc2.read_points(cloud, skip_nans=True):
        rgb_list = float_to_rgb(point[3])
        if using_hsv:
            point_colors_list.append(rgb_to_hsv(rgb_list) * 255)
        else:
            point_colors_list.append(rgb_list)

    # Populate lists with color values
    channel_1_vals = []
    channel_2_vals = []
    channel_3_vals = []

    for color in point_colors_list:
        channel_1_vals.append(color[0])
        channel_2_vals.append(color[1])
        channel_3_vals.append(color[2])
    
    # Compute histograms
    nbins = 64
    bins_range = (0, 256)
    channel_1_hist, _ = np.histogram(channel_1_vals, bins=nbins, range=bins_range)
    channel_2_hist, _ = np.histogram(channel_2_vals, bins=nbins, range=bins_range)
    channel_3_hist, _ = np.histogram(channel_3_vals, bins=nbins, range=bins_range)

    # print('channel_1_hist:', channel_1_hist)
    # print('channel_2_hist:', channel_2_hist)
    # print('channel_3_hist:', channel_3_hist)
    # Concatenate and normalize the histograms
    hist_features = np.concatenate((channel_1_hist,
                                    channel_2_hist,
                                    channel_3_hist)).astype(np.float64)
    # Generate random features for demo mode.  
    # Replace normed_features with your feature vector
    normed_features = hist_features / np.sum(hist_features)
    return normed_features 


def compute_normal_histograms(normal_cloud):
    norm_x_vals = []
    norm_y_vals = []
    norm_z_vals = []

    for norm_component in pc2.read_points(normal_cloud,
                                          field_names = ('normal_x', 'normal_y', 'normal_z'),
                                          skip_nans=True):
        norm_x_vals.append(norm_component[0])
        norm_y_vals.append(norm_component[1])
        norm_z_vals.append(norm_component[2])

    # print('norm_x_vals:', np.amin(norm_x_vals), np.amax(norm_x_vals))
    # print('norm_y_vals:', np.amin(norm_y_vals), np.amax(norm_y_vals))
    # print('norm_z_vals:', np.amin(norm_z_vals), np.amax(norm_z_vals))
    # Compute histograms of normal values (just like with color)
    nbins = 50
    bins_range = (-1, 1)
    norm_x_hist, _ = np.histogram(norm_x_vals, bins=nbins, range=bins_range)
    norm_y_hist, _ = np.histogram(norm_y_vals, bins=nbins, range=bins_range)
    norm_z_hist, _ = np.histogram(norm_z_vals, bins=nbins, range=bins_range)

    # Concatenate and normalize the histograms
    hist_features = np.concatenate((norm_x_hist,
                                    norm_y_hist,
                                    norm_z_hist)).astype(np.float64)

    # Generate random features for demo mode.  
    # Replace normed_features with your feature vector
    normed_features = hist_features / np.sum(hist_features)

    return normed_features
```

The implementation of object recognition is as follows:
```python
    # Classify the clusters! (loop through each detected cluster one at a time)
    detected_objects_labels = []
    detected_objects = []
    detected_objects_pos = []

    for index, pts_list in enumerate(cluster_indices):
        # Grab the points for the cluster from the extracted outliers (cloud_objects)
        pcl_cluster = pcl_objects.extract(pts_list)

        # Convert the cluster from pcl to ROS using helper function
        ros_cluster = pcl_to_ros(pcl_cluster)

        # Compute the associated feature vector
        chists = compute_color_histograms(ros_cluster, using_hsv=True)
        normals = get_normals(ros_cluster)
        nhists = compute_normal_histograms(normals)
        feature = np.concatenate((chists, nhists))

        # Make the prediction
        feature = feature.reshape(1, -1)
        prediction = clf.predict(scaler.transform(feature))
        label = encoder.inverse_transform(prediction)[0]
        detected_objects_labels.append(label)

        # Publish a label into RViz
        label_pos = list(white_cloud[pts_list[0]])
        detected_objects_pos.append(label_pos)
        pcl_object_markers_pub.publish(make_label(label, label_pos, index))

        # Add the detected object to the list of detected objects.
        do = DetectedObject()
        do.label = label
        do.cloud = ros_cluster
        detected_objects.append(do)
    # End for index, pts_list in enumerate(cluster_indices)
```

### Pick and Place Setup

#### 1. For all three tabletop setups (`test*.world`), perform object recognition, then read in respective pick list (`pick_list_*.yaml`). Next construct the messages that would comprise a valid `PickPlace` request output them to `.yaml` format.

Here I paste screenshots of each world after each filter and final labeled camera photo

 1. World 1
   - Voxel downsampled: ![world_1_voxel][world_1_voxel]
   - Passthrough filtered: ![world_1_pass][world_1_pass]
   - Noise filtered: ![world_1_noise][world_1_noise]
   - Final labeled camera image: ![world_1_final][world_1_final]
 1. World 2
   - Voxel downsampled: ![world_2_voxel][world_2_voxel]
   - Passthrough filtered: ![world_2_pass][world_2_pass]
   - Noise filtered: ![world_2_noise][world_2_noise]
   - Final labeled camera image: ![world_2_final][world_2_final]
 1. World 3
   - Voxel downsampled: ![world_3_voxel][world_3_voxel]
   - Passthrough filtered: ![world_3_pass][world_3_pass]
   - Noise filtered: ![world_3_noise][world_3_noise]
   - Final labeled camera image: ![world_3_final][world_3_final]


### Discussions
A few improvements that I did:

 1. During RANSAC filtering, since the camera has a wide angle, the two dropboxes are partially being caught, in order to avoid confusing the robot, I added a y-axis passthrough filter with range of (-0.5, 0.5) to remove the dropboxes
 2. During clustering, I changed the `MinClusterSize` to adopt different sizes of smaller items of different test world. Otherwise, I see the SVM classifier labels a big item, e.g. biscuits box as two smaller items, e.g. soap, soap2, (when MinClusterSize is too small), or, misses small items (when MinClusterSize is too big)
 3. In test world 3, it is difficult to detect the glue since it's only partially visible. And during training, I do not have partially observable data.
